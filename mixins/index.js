'use strict';

var assert = require('assert');

function mixin (destination /* mixins */) {
  var mixins = Array.prototype.slice.call(arguments, 1);

  mixins.forEach(function (mixin) {
    Object.keys(mixin).forEach(function (key) {
      // this silently overrwrites things on the prototype
      // you could enforce that it does not by:
      // if (destination.prototype[key] throw) new Error('already exists');
      destination.prototype[key] = mixin[key];
    });
  });

  return destination;
};

module.exports = mixin;

function BankAccount (startingBalance) {
  this.balance = startingBalance || 0;
  this.routingNumber = '552223344';
  this.accountNumber = '1234';
}

BankAccount.prototype.sendBalanceToServer = function () {
  /* ... connect to banking service ...*/
};

BankAccount.prototype.charge = function (amount) {
  this.debit(amount);
  this.sendBalanceToServer(this.routingNumber, this.accountNumber);
}

function LunchesBoughtForSeanLedger () {
  this.balance = 0;
}

var balanceUtil = {
  debit: function (amount) {
    this.balance = this.balance - amount;
  },
  credit: function (amount) {
    this.balance = this.balance + amount;
  }
};

var loudUtil = {
  foo: function () {
    return 'bar'
  }
};

mixin(BankAccount, balanceUtil);
mixin(LunchesBoughtForSeanLedger, balanceUtil);
mixin(BankAccount, loudUtil);

var myAccount = new BankAccount(1337);
myAccount.credit(2663);
assert.equal(myAccount.foo(), 'bar');
assert.equal(myAccount.balance, 4000);

var lunchesSinceJanuary = new LunchesBoughtForSeanLedger();
lunchesSinceJanuary.credit(10);
lunchesSinceJanuary.debit(5);
assert.equal(lunchesSinceJanuary.balance, 5);
