'use strict';

var assert = require('assert');
var Account = require('./account');
var USAccount = require('./us-account');
var SpecialUSAccount = require('./special-us-account');

var account = new Account(1);
var usAccount = new USAccount(100);
var specialAccount = new SpecialUSAccount();
var hackedUsAccount = new USAccount(100);

hackedUsAccount.getBalance = function () {
  return 'hacked';
}

assert.equal(usAccount.getBalance(), 1);
assert.equal(hackedUsAccount.getBalance(), 'hacked');
var specialAccount = new SpecialUSAccount();


// now, lets examine what happens we we call
specialAccount.getBalance();

/* specialAccount.getBalance()
- the runtime looks at specialAccount, which is just an object, and asks
  > do you have a function called getBalance?
    *nope*
      - the runtime looks up the prototype chain to USAccount, which is just an object, and asks
          > do _you_ have a function getBalance?
          *nope*
            - the runtime looks up the prototype chain to Account, which is just an object, and asks
              -> Account
                > do _you_ have a function getBalance?
                *yes*
                  - Account.prototype.getBalance is invoked, with the value of 'this' set to specialAccount, and the balance of
                    special account is returned.

specialAccount -> SpecialUSAccount.prototype -> USAccount.prototype -> Account.prototype -> (Function.prototype) -> (Object.prototype)
*/

// let's see what happens when we call .hasOwnProperty
specialAccount.hasOwnProperty('hasOwnProperty');

specialAccount -> SpecialUSAccount.prototype -> USAccount.prototype -> Account.prototype -> (Function.prototype) -> (Object.prototype)
