'use strict';

var assert = require('assert');
var USAccount = require('./us-account');

function USSpecialAccount (mount) {
  var startingAmount = 1000000;
  var rest = Array.prototype.slice.call(arguments, 1);

  USAccount.apply(this, [startingAmount].concat(rest));
}

USSpecialAccount.prototype = Object.create(USAccount.prototype);

USSpecialAccount.prototype.magicNumber = function () {
  return 1337;
};

var myAccount = new USSpecialAccount(10);

myAccount.credit(5);

assert.equal(myAccount.getBalance(), 1000005);
assert.equal(myAccount.getExchangeRate(),  'not good');
assert.equal(myAccount.magicNumber(),  1337);

module.exports = USSpecialAccount;
