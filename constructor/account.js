'use strict';

var assert = require('assert');

function Account (startingBalance) {
  this.balance = startingBalance || 0;
};

Account.prototype.credit = function (amount) {
  this.balance = this.balance + amount;
};

Account.prototype.debit = function (amount) {
  this.balance = this.balance - amount;
};

Account.prototype.getBalance = function () {
  return this.balance;
};

var myAccount = new Account();
var mySecondAccount = new Account();

myAccount.credit(10);
myAccount.debit(5);
assert.equal(myAccount.getBalance(), 5);

mySecondAccount.credit(10);
mySecondAccount.credit(5);
assert.equal(mySecondAccount.getBalance(), 15);

module.exports = Account;
