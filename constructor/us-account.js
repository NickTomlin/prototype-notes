'use strict';

var assert = require('assert');
var Account = require('./account');

function USAccount () {
  Account.apply(this, arguments);
}

USAccount.prototype = Object.create(Account.prototype);

USAccount.prototype.getExchangeRate = function () {
  return 'not good';
};

var myAccount = new USAccount(10);

myAccount.credit(5);
assert.equal(myAccount.getBalance(), 15);
assert.equal(myAccount.getExchangeRate(),  'not good');

module.exports = USAccount;
