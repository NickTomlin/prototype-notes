'use strict';

var assert = require('assert');

var Account = function (startingBalance) {
  var balance = startingBalance || 0;

  function credit (amount) {
    balance = balance + amount;
  }

  function debit (amount) {
    balance = balance - amount;
  }

  function getBalance (account) {
    return balance;
  }

  return {
    credit: credit,
    debit: debit,
    getBalance: getBalance
  };
}

var accountOne = Account(5);
var accountTwo = Account(10);

accountOne.credit(10);
accountOne.debit(5);

accountTwo.credit(10);
accountTwo.debit(19);

assert.equal(accountOne.getBalance(), 10);
assert.equal(accountTwo.getBalance(), 1);
