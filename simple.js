'use strict';

var assert = require('assert');

function creditAccount (account, amount) {
  account.balance = account.balance + amount;
}

function debitAccount (account, amount) {
  account.balance = account.balance - amount;
}

function getAccountBalance (account) {
  return account.balance;
}

var myAccount = {
  balance: 0
};

creditAccount(myAccount, 10);
debitAccount(myAccount, 5);

assert.equal(getAccountBalance(myAccount), 5);
