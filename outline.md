Notes on prototypes !!!SUPER HACKY WIP!!!
---

# Intro: It's not about prototypes, it's about inheritance

This is where I want to talk about inheritance, the why and a little bit of the how

We have a bank account. That's great. It's an object with some methods that maintain an internal `balance`. We can do this any number of ways in Javascript.

But... we need to create European and American bank accounts to easily translate things.

This is the point where we want to think about inheritance ...

** Almost all inheritance examples are flawed. Including the ones I give **

- All examples are going to fall short.
- It's hard to reason about until you need it
- The core drive of sharing code is the same, and that should be your focus.

The goal: reduce duplication in the manner that best suits you and your team. That happens to be prototypes for my team: testability.

# Traditional inheritance
```
require('./constructors')
```

# The prototype

``` 
myAccount <- US-Bank Account <- Account
             => getEuros
                        => getBalance
```

## Everything has a prototype

The humble `{}`

- Everything inherits from `Object.prototype`

Only Constructors have a `.prototype`, but all objects have a hidden `[[prototype]]` that they inherit from.

```
require('./monkey-patching')
```

# Why not just define functions?

- You totally can just use closures for everything.
  `require('./closures')`
- The prototype is more performant (technically) you are making a reference instead of defining a new function

# Other methods of inheritance

```
require('./mixins')
```

# Toolchest

`instanceof` - `{} instanceof Object`
`typeof` - compare primitives (beware that this can lie)
`Object.getPrototypeOf` - In modern browsers
