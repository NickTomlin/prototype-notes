'use strict';

var assert = require('assert');

var first = Object.create({
  original: function () {
    return 'original'
  }
});

var second = Object.create(first);

second.second = function () {
  return 'second';
};

var third = Object.create(second, {
  foo: {
    value: function () {
      return 'foo';
    }
  }
});


// third.original?
//    nope.
//    -> second.original?
//      nope.
//       -> first.original?
//         yep!

assert.equal(third.original(), 'original');
assert.equal(second.original(), 'original');
assert.equal(Object.getPrototypeOf(second), first);
assert.equal(second.hasOwnProperty('original'), false);
assert.equal(third.foo(), 'foo');
