'use strict';

var assert = require('assert');

/*
!!! BEWARE the dangers of modifying built-in prototypes !!!
*/

String.prototype.sarcasm = function () {
  return '<sarcasm>' + this + '</sarcasm>';
};

assert.equal('i love your hair'.sarcasm(), '<sarcasm>i love your hair</sarcasm>');

Object.prototype.isAwesome = function () {
  return /haxfred/.test(this.toString());
};

function foo () {}
function haxfred () {}

assert(foo.isAwesome() === false);
assert(haxfred.isAwesome());
assert(['haxfred'].isAwesome());
assert(['notHaxfred'].isAwesome() === false);
